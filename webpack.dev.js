// libs import
const webpack = require( 'webpack' );
const merge   = require( 'webpack-merge' );

const {
    pipe,
} = require( 'ramda' );

const {
    plugin
} = require( 'webpack-partial' );

// configs import
const {
    commonBaseConfig,
    commonBasePlugins
}  = require( './webpack.common.js' );

// loaders import
const babel = require( './config/webpack/loaders/babel-loader' );

const {
    css
} = require( './config/webpack/loaders/style-loaders' );

const {
    fontLoader,
    imageLoader,
    htmlLoader
} = require( './config/webpack/loaders/other-loaders' );

// plugins import
const {
    browserSyncPlugin
} = require( './config/webpack/plugins/plugins' );

// compose plugins
const plugins = pipe(
    browserSyncPlugin(),
    plugin( new webpack.HotModuleReplacementPlugin() )
);

// compose loaders
const loaders = pipe(
    css(),
    babel(),
    htmlLoader(),

    fontLoader({ limit: 100000 }),

    imageLoader({
        name: '[name].[ext]',
        outputPath: 'img/',
        publicPath: 'img/'
    })
);

const buildDevConfig = pipe(
    plugins,
    loaders
);

const baseDevConfig = {
    mode: 'development',

    devtool: 'inline-source-map',

    entry: [
        './src/index.js'
    ],

    output: {
        filename: '[name].js'
    },

    watch: true,

    devServer: {
        host: 'localhost',
        port: 3100,
        hot: true,

        // TODO PB Potencjalny hack, zbadaj potem.
        watchOptions: {
            poll: true
        }
    }
};

module.exports = merge(
    commonBaseConfig,
    commonBasePlugins,
    buildDevConfig( baseDevConfig )
);
