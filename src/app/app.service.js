import {
    addItem,
    removeItem,
    toggleDone
} from './actions'

export default class ProvideListItem {
    constructor($ngRedux) {
        Object.assign(this, { $ngRedux })

        $ngRedux.connect(this.mapStateToThis)(this)
    }

    mapStateToThis(state) {
        const { todos } = state

        return {
            todos
        }
    }

    add(title) {
        if (!title) {
            return
        }

        this.$ngRedux
            .dispatch(
                addItem( title )
            )
    }

    toggleDone(id) {
        this.$ngRedux
            .dispatch(
                toggleDone( id )
            )
    }

    remove() {
        this.$ngRedux
            .dispatch(
                removeItem()
            )
    }

    remaining() {
        return this.getList()
            .filter( item => item.done )
            .length
    }

    getList() {
        return this.todos
    }
}