import Chance from 'chance'

import {
    ADD_ITEM,
    REMOVE_ITEM,
    TOGGLE_ITEM
} from '../actions/constants'

const initialState = [
    {
        id: new Chance().guid(),
        title: 'zadanie1',
        done: true
    },
    {
        id: new Chance().guid(),
        title: 'zadanie',
        done: false
    }
]

function todosReducer(state = initialState, action) {
    switch (action.type) {

        case ADD_ITEM:
            return [
                ...state,
                action.item
            ]

        case REMOVE_ITEM:
            return state
                .filter(item => !item.done)

        case TOGGLE_ITEM:
            return state
                .map(
                    item => item.id === action.id ?
                        { ...item, done: !item.done } : item
                )

        default:
            return state
    }
}

export default todosReducer