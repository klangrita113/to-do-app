import { createLogger } from 'redux-logger'
import reducers from './reducers'

export default function reduxConfig( $ngReduxProvider ) {
    const middlewares = [
        createLogger({ diff: true, collapsed: true })
    ]

    $ngReduxProvider
        .createStoreWith( reducers, middlewares )
}