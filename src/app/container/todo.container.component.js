import template from './todo.template.html'

class controller {
    constructor(ProvideListItem) {
        Object.assign(this, {
            ProvideListItem
        })
    }
    /*
        wartstwa abstrakcji dla serwisu ProvideListItem,
        np. przy zmianie nazwy serwisu zmieniamy ją tylko w tym miejscu
    */
    _listManager() {
        return this.ProvideListItem
    }

    remaining(list) {
        return this._listManager().remaining(list)
    }

    toggleDone(id) {
        return this._listManager().toggleDone(id)
    } 

    remove() {
        return this._listManager().remove()
    }

    add(newTodo) {
        return this._listManager().add(newTodo)
    }

    getList() {
        return this._listManager().getList()
    }
}
export default {
    template,
    controller
}