import template from './todoitem.template.html'

export default {
    template,
    bindings: {
        item: '<',
        toggle: '&'
    }
}
