import template from './todolist.template.html'

export default {
    template,
    bindings: {
        list: '<',
        toggle: '&'
    }
}

