import template from './todoadd.template.html'

export default{
    template,
    bindings: {
        add: '&',
        newTodo: '<'
    }
};
