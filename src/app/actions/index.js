import Chance from 'chance'

import {
    ADD_ITEM,
    REMOVE_ITEM,
    TOGGLE_ITEM
} from './constants'

export const addItem = title => ({
    type: ADD_ITEM,
    item: {
        id: new Chance().guid(),
        title,
        done: false
    }
})

export const toggleDone = id => ({
    type: TOGGLE_ITEM,
    id
})

export const removeItem = () => ({
    type: REMOVE_ITEM
})