import angular from 'angular'
import ngRedux from 'ng-redux'

import reduxConfig from './app-redux.config'
import ProvideListItem from './app.service'
import appRootComponent from './app.root.component'
import toDoContainerComponent from './container/todo.container.component'
import toDoAddComponent from'./container/to-do-add/todoadd.component'
import toDoListComponent from './container/to-do-list/todolist.component'
import toDoItemComponent from './container/to-do-list/to-do-item/todoitem.component'

const DEPENDENCIES = [
    ngRedux
]

export default angular
    .module('ToDoApp', DEPENDENCIES)
    .service('ProvideListItem', ProvideListItem)
    .component('appRoot', appRootComponent)
    .component('toDoContainer', toDoContainerComponent)
    .component('toDoAdd', toDoAddComponent)
    .component('toDoList', toDoListComponent)
    .component('toDoItem', toDoItemComponent)
    .config(reduxConfig)